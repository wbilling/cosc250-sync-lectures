
## Question 4 [10 marks]

a) What does it mean to say that a function is *pure*, and what is meant by *referential transparency*? Give an example in your answer. [2 marks]

b) Explain, with an example, what *tail recursion* is and when it can be achieved? [2 marks]

c) What does it mean for a calculation to be *lazy* or *strict*? [2 marks]

d) What is a *monad*? Include the monad laws in your answer, and *give* two examples of monads from the Scala libraries. [4 marks] 

## Question 5 [8 marks]

Pascal's triangle begins as follows:

```
        1
      1   1
    1   2   1
  1   2   2   1
1   3   4   3   1
```

Each number is the sum of the two numbers above it in the immediately preceeding row.

Write a recursive function *fib(n)* that will calculate the *n*th row of Pascal's triangle.

```scala
def fib(n:Int):Seq[Int] = ???
```

## Question 6 [6 marks]

a) What is a `Future` in Scala? Explain, with an example, what is it useful for, and how it relates to `Promise`. [2 marks]

b) What is an *Actor*? Give an example of how interaction between Actors might use Futures. [4 marks]

c) What is *backpressure* in a reactive stream, and why is it needed? [2 marks]

## Question 7 [6 marks]

Determine the result and the return type of the following code. `Source` and `Sink` are imported from Akka Streams, but `Stream` is from the Scala standard library

a) `List(1, 2, 3).zipWithIndex.map({ case (x, y) => x * y})` [1 mark]

b) `List(1, 2, 3).flatMap({ x => List(x, x)})` [1 mark]

b) `Stream.from(1)` [1 mark]

c) `Stream.from(1).map(_ * 2)` [1 mark]

d) `Source(1 to 10).to(Sink.fold[Int, Int](0)(_ + _))` [2 marks]







