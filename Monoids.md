
class: center, middle

# Monoids and Composition

No, they're not a Doctor Who monster...

---

### Multiplication

I suspect you've noticed:

--

* 1 * x == x

--

* x * 1 == 1

--

Let's call 1 the *identity element* for multiplication

---

### Multiplication

I suspect you've noticed:

--

* (2 \* 3) \* 4 == 6 * 4 == 24

--

* 2 \* (3 \* 4) == 2 * 12 == 24

--

In fact, 

* (a \* b) \* c == a \* (b \* c)

--

So much so that we can leave the parentheses off and just write

* a \* b \* c

--

Multiplication over integers is *associative*

---

### We could do this for addition too

I suspect you've noticed:

--

* 0 + x == x

--

* x + 0 == x

--

Let's call 0 the *identity element* for addition

---

### Addition


I suspect you've noticed:

--

* (2 + 3) + 4 == 5 + 4 == 9

--

* 2 + (3 + 4) == 2 + 7 == 9

--

In fact, 

* (a + b) + c == a + (b + c)

--

So much so that we can leave the parentheses off and just write

* a + b + c

--

Addition over integers is *associative*

---

class: centre, middle

### News is, we can do this for some types and functions too...

---

### Concatenation for Lists

Concatenation uses the `++` operator:

```scala
List(1, 2, 3) ++ List(4, 5) == List(1, 2, 3, 4, 5)
```

You might have noticed:

--

* List.empty ++ listA == listA

--

* listA ++ List.empty == listA

--

Let's call the empty list the *identity element* for list concatenation


---

### Concatenation for Lists

You might have noticed:

--

* (List(1, 2) ++ List(3, 4)) ++ List(5, 6) == List(1, 2, 3, 4, 5, 6)

--

* List(1, 2) ++ (List(3, 4) ++ List(5, 6)) == List(1, 2, 3, 4, 5, 6)

--

in fact, 

* (listA ++ listB) + listC == listA + (listB ++ listC)

--

So much so we can just drop the brackets and say

* listA ++ listB ++ listC


---

### What do these have in common

* *addition* over *integers* with *zero* as an identity element
* *multiplication* over *integers* with *one* as an identity element
* *concatenation* over *lists* with *the empty list (Nil)* as an identity element

--
* *concatenation* over *strings* with `""` as an identity element

--

They're all *associative*, but that doesn't capture the part about   
*identity + x == x == x + identity*

--

Let's give them a name. Let's call them ***monoids***

---

### Monoids

What we need for a monoid:

* A type `T`
* An identity element. Let's call it `mempty` for the slide
* An operation that takes two arguments of type `T`. Let's call it `<>` for the slide.

--

And these rules

```scala
mempty <> x == x // left identity
```

```scala
x <> mempty == x // right identity
```

```scala
(a <> b) <> c == a <> (b <> c) // associativity
```

---

### In Haskell

```haskell
class Monoid a where
    mempty  :: a
    mappend :: a -> a -> a

    mconcat :: [a] -> a
    mconcat = foldr mappend mempty
```

--

Things to point out:

* The type system can't enforce the laws (left identity, right identity, associativity)

--

* It's a typeclass 

--

* mconcat...

---

### mconcat

Suppose we have

```
a <> b <> c <> d <> e
```

That looks an awful lot like our example of summing across lists:

```
0 + a + b + c + d + e
```

which we did using fold

```
(((((0 + a) + b) + c) + d) + e)
```

--

It looks like it ought to be possible to say something like

```
List(a, b, c, d, e).foldLeft(mempty)(<>)
```

---

### mconcat

And that's what Haskell does, except using fold right instead of left. 

```
    mconcat :: [a] -> a
    mconcat = foldr mappend mempty
```

--

But remember, `mappend` is associative, so it doesn't matter where we put the brackets. So whether we use `foldl` or `foldr` doesn't matter.

---

### Summary

* Monoids are a type, an operation, and an identity element such that the monoid laws hold

* Monoids let us compose operations without worrying about the order of the brackets.

* That's a nice property -- just knowing things compose well -- so often we'd like operations to be monoid-like





