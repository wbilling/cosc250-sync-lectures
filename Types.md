class: center, middle

# Types

.byline[ *Will Billingsley* ]

---

class: bigquote

## Let's start with some sums...

--

> 1 plus 1 equals 

--
2

--

> red plus green equals 

--
yellow

--

> 1 plus red equals 

--
?

---

class: bigquote

## What about...

--

> red plus rabbit equals

--
a red rabbit

--

> rabbit plus rabbit equals 

--
lots of baby rabbits

---

## Let's do something a little engineering

--

3V &times; ((12V / 6&Omega;) + 2A)

--

= 3V &times; ((12V / 6(V/A)) + 2A)

--

= 3V &times; ((12 V&times;A / 6V) + 2A)

--

= 3V &times; ((12 A / 6) + 2 A)

--

= 3V &times; (2A + 2A)

--

= 3V &times; 4A

--

= 3 &times; 4 A &times; V

--

= 12 W

---

## The message here...

* Something as simple as `a + b` can mean different things depending on what type `a` and `b` are

--

* When a program encounters `a + b`, it needs to know what they are to decide what to do

--

* By tracking the types, as programmers we can help ourselves avoid mistakes

---

## Different type systems

* *When* do we check the types?

   - compile time?  "Static typing"
   - run time?  "Dynamic typing"

---

## Languages and types

<table>
  <thead><tr><th></th><th>Imperative language</th><th>Functional language</th></tr></thead>
  <tbody>
    <tr>
      <th>Dynamically typed</th>
      <td>
        JavaScript <br />
        Python
      </td>
      <td>
        Lisp <br />
        Scheme <br />
        Clojure <br />
      </td>
    </tr>  
    <tr>
      <th>Statically typed</th>
      <td>
        Java <br />
        C<br/> C++<br/> C#
      </td>
      <td>
        Haskell <br />
        OCaml <br />
        Scala <br />
      </td>
    </tr>
  </tbody>
</table>

---

## Static typing can help  performance

Suppose I say 

```
var c = a + b
```

Is that integer addition or string append?

In a dynamically typed language, the computer needs to inspect at runtime to know.

In a compiled language, the compiler can decide ahead of time.

---

## A successful checker

* Lets us express every program we want

* Stops us from expressing every program we obviously *didn't* want

--

   - Fixing a bug in production costs 10 times as much as before I've released it

---

## Type inference

* Let's keep the benefits of static typing  
  *finding errors sooner, tool support, eg, auto-completion in IDEs*  
  But lose some of the verbosity.

* What type are these?

--
  ```scala
  val a = 127
  ```
--
  ```scala
  val b = "Hello world"
  ```  
--
  ```scala
  val b = List("one", "two", "three").head
  ```  
--
* Type inference -- so easy, you can do it already!  
  

---
  
## Type inference gotchas

* Sometimes it needs some help

  ```scala
  def factorialStep(soFar:Long, thisNum:Int) = {
      if (thisNum == 1) {
        soFar
      } else {
        factorialStep(thisNum * soFar, thisNum - 1)
      }
  }
  ```
  
  ```
  [error] StepOne.scala:63: recursive method factorialStep needs result type
  [error]         factorialStep(thisNum * soFar, thisNum - 1)
  [error]         ^
  ```  
  
---

## Type inference gotchas

* Sometimes it needs some help

  ```scala
  def factorialStep(soFar:Long, thisNum:Int):Long = {
      if (thisNum == 1) {
        soFar
      } else {
        factorialStep(thisNum * soFar, thisNum - 1)
      }
  }
  ```
---

  
## Type inference gotchas

Sometimes it can be too narrow

```scala
trait ContentItem {
  def copyrightHolder = None
}

class Book(val title:String, val author:String) extends ContentItem {
  override def copyrightHolder = author
}
```

```
Expression of type Some[String] does not conform to expected type None.type
```

---

## Strong versus Weak typing

* *What do we do* if we find the type is incompatible?

   - throw an error? "Strong typing"
   - fudge it somehow? "Weak typing"

---

## Weak typing in JavaScript

What should these resolve to?

```js
if (3 ^ "apples" == 4 ^ "oranges") { console.log("Yes, it does!") }
```

--

```js
a == !!a 
```

--

```js
17 + ("apples" != "oranges") + "foo"
```

---

## C is statically but weakly typed

```c
int main () {
    char* pointerToString = "abcd";
    char character = 'e';
    (int)pointerToString + (int)character;
}
```

---

## Strong v Weak typing

Generally, we prefer strong typing as it means our program behaves predictably

But sometimes we do want a conversion to take place. Should it be legal to call this
function with an `Int`?

```scala
// Returns the minimum width of bits needed to represented this number.
// eg, 3 can be represented in 2 bits, but 4 requires 3 bits
def minBits(x:Long) = {
  Math.ceil(Math.log(x + 1) / Math.log(2)).toInt
}
```

--

Scala provides a way of defining what conversions the compiler should implicitly perform

---

## Implicit conversion

Suppose I wanted Scala to auto-convert `Int` to `String`

```scala
def hello(name:String) = println(s"Hello $name")
hello(123)
```

By default, this won't compile. But if there is an implicit conversion in scope, it can:

--
```scala
implicit def intToStr(i:Int):String = i.toString

def hello(name:String) = println(s"Hello $name")
hello(123)
```

Scala uses this, for example, to autoconvert between its representation of `Int` and Java's `Integer` -- the implicit conversions are defined in `scala.Predef`

---

## Implicit conversions

Another use of this is if we'd like to add a missing method to a class. 

For example, Scala lets us treat Strings as if they are arrays of Chars 

```scala
"hello"(3)
```

```scala
"ibm".map({ case x:Char => (x - 1).toChar })
```

Scala implements these by performing an *implicit conversion* from `String` (which doesn't have these methods) to `StringOps` (which does)

This is done safely, at compile time.

---

## Implicit conversions

Let's define one of our own. A function that will make anything sound more refined...

To start with, this doesn't compile:

```scala
"hello".chappify
```

--

Let's define a class that has the method we want

```scala
class Chappify(val s:String) {
  def chappify = s + ", dear chap"
}
```

--

Now we can use the class, but we have to do it explicitly:

```scala
new Chappify("hello").chappify
```

---

## Making the implicit conversion

Let's declare an *implicit conversion* to `Chappify`

```scala
implicit def strToChappify(s:String):Chappify = new Chappify(s)
```

--

Now, this works:

```scala
"hello".chappify
```

```
hello, dear chap
```

--

The conversion code is being inserted *at compile time*. The compiler didn't find our requested method on our class, but did find an implicit function that could convert it to a class that has that method.

```scala
strToChappify("hello").chappify
```

---

## Domain specific languages

--
Scala lets us drop brackets and dots in certain circumstances.

eg:

```scala
"ibm".map({ x:Char => (x - 1).toChar })
```

can be written

```scala
"ibm" map { x:Char => (x - 1).toChar }
```

---

## Domain specific languages

This combination, of 

* being able to (seemingly) add methods to a class, eg string

* being able to omit dots and brackets

makes it possible to write *domain specific languages* -- specialised notations for a particular purpose.

For example, unit tests

---

## What's going on in your Spec

ScalaTest, the testing framework used in your assignment, uses this to make test code more readable.

```scala
"doubleArray" should "double the contents of an array" in {
  doubleArray(Array(1, 2, 3)) should be (Array(2, 4, 6))
  doubleArray(Array(9, 7, 5, 12)) should be (Array(18, 14, 10, 24))
}
```

If we control click on `should` or `in` we'll end up in a very curious class...

There is a tradeoff that the code *using* the DSL is readable, but the code *implementing* the DSL is more opaque.

---

## Our own DSL

For fun, let's make this compile and work:

```scala
"one" plus "one" equals "two"
```

--
Let's assume we have a couple of pre-defined maps that can take a number and give you a string, or take a string and give you a number:

```scala
// A map from our strings to numbers
val sToI = Map(
  "one" -> 1,
  "two" -> 2,
  "three" -> 3
)

// This is a sneaky quick way to derive the reverse map (from numbers to strings)
val iToS = sToI map { case (k,v) => (v,k) }
```

---

### Our own DSL

First, we define an augmented string wrapper with the method we want

```scala
class AddStr(s:String) {  
  def plus(s2:String): String = {
    iToS(sToI(s) + sToI(s2))
  }
}
```

--
Then, an implict conversion to promote strings to it

```scala
implicit def strToAddStr(s:String) = new AddStr(s)
```

--
Now this should work

```scala
"one" plus "one" equals "two" // true
"one" plus "two" // three
```