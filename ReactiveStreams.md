class: center, middle


# Reactive Streams

.byline[ *Will Billingsley, CC-BY* ]

---

## First, a warning

There are *two* kinds of `Stream` we're going to talk about in this unit. 

This week we are talking about *Reactive Streams* using Akka Streams. These are **different** from the `scala.collection.immutable.Stream` type which we met last week.


---

## Back to the Futures...

* Over the last week or so, we've talked about `Future[T]` -- at some point in the *future* this will contain a single `T`.

--

* But what if we want more than one `T`?  
  eg, request all tweets with hashtag #scala
  
  What do we return? `Future[Seq[Tweet]]`?  
  that could be a pretty big Seq...

---

## What is the plural of `Future`?

* `Future[Seq[T]]` would have to all become available at once

--

* `Seq[Future[T]]` would imply we know how many there are going to be

--

* `Stream[Future[T]]` is problematic because we don't want to cache every previous value

---

## When we say stream...

![Stream](cosc250/water-wet-river-nature-60685.jpeg)

---

## When we say stream...

![Stream](cosc250/person-stream-cliff-river.jpg)

---

## The Reactive Manifesto

http://www.reactivemanifesto.org/

> application requirements have changed dramatically in recent years. Only a few years ago a large application had tens of servers, seconds of response time, hours of offline maintenance and gigabytes of data. Today applications are deployed on everything from mobile devices to cloud-based clusters running thousands of multi-core processors. Users expect millisecond response times and 100% uptime. Data is measured in Petabytes. Today's demands are simply not met by yesterday’s software architectures.

---

## The Reactive Manifesto

> we want systems that are **Responsive**, **Resilient**, **Elastic** and **Message Driven**. We call these Reactive Systems.


---



class: center, middle

### Q: How do floods happen?

---

class: center, middle

### Q: What about data floods?

---

### The need for *backpressure*

* Suppose we make a request for data to a really fast back-end API, over the network

--

* ...it might start writing out data faster than we can process it...

--

* ...in which case we're going to need to buffer or drop the data...

--

* ...but our buffers could get bigger and bigger until we run out of memory...

--

* *CRASH!*

---

class: center, middle

### Imagine if I asked you to give me some rope...

---

class: center, middle

### Imagine instead I asked you to *feed* me some rope...

---

### Backpressure

* The subscriber asks the publisher *give me X more items*

* The publisher writes them out and *stops until the subscriber asks for more*

* Supply is never allowed to be greater than demand

---

### Java API

Most methods in the interfaces return `void`. This is because it is a Java-style of asynchronicity. The publisher will call you back later...

```java
interface Publisher<T> {
  void subscribe(Subscriber<? super T> s);
}
```

---

### Subscriber

Once a subscriber has subscribed, the publisher will send it a `Subscription`

```java
interface Subscriber<T> {

  void onSubscription(Subscription s);

  void onComplete();
  
  void onError(Throwable t);
  
  void onNext(T item);    
}
```

---

### Subscription

The subscription is how the subscriber controls the backpressure

```java
interafce Subscription {

  void request(long n);

  void cancel();
  
}
```

---

### An assembly line analogy

We now have Publishers that can produce data, and Subscribers that can consume them. But often we want to do things to the data along the way. So we need something that can Subscribe to data, process it, and then Publish the processed version

```java
interface Processor<T, R> extends Subscriber<T>, Publisher<R>
```

---

class: center, middle

### Ok, but that's *Java*. I want my maps!

---

### Akka Streams

* An implementation of Reactive Streams that lets you write in more idiomatic Scala.

--

* `Source[T, NotUsed]` represents a source of data

--

* `Sink[T, NotUsed]` represents where data can flow to

--

* `Flow[In, Out, NotUsed]` can sit in the middle

--

* We can *describe a flow without running it yet*

---

### Akka streams example

This describes a computation on streams, but we haven't tied it to a sink, and we haven't started it running

```scala
val authors: Source[Author, NotUsed] =
  tweets
    .filter(_.hashtags.contains(myTag))
    .map(_.author)
```

---

### Materialising streams

* Attach it to a sink

* "Materialise" it over an actor system. (Because it's got to be processed somewhere!)

---

### Materialisation example

From the Akka docs

```scala
val sumSink = Sink.fold[Int, Int](0)(_ + _)
val counterRunnableGraph: RunnableGraph[Future[Int]] =
  tweetsInMinuteFromNow
    .filter(_.hashtags contains akkaTag)
    .map(t => 1)
    .toMat(sumSink)(Keep.right)
 
// materialize the stream once in the morning
val morningTweetsCount: Future[Int] = counterRunnableGraph.run()
// and once in the evening, reusing the flow
val eveningTweetsCount: Future[Int] = counterRunnableGraph.run()
```


