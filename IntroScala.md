class: center, middle

# An imperative intro to Scala

.byline[ *Will Billingsley, CC-BY* ]

---

### Scala compile targets

* Compiles to run on the Java Virtual Machine (JVM)

* Scala.js compiles to JavaScript (Node.js or browsers)

* Scala Native compiles to native machine code

---

### Expression-oriented

Everything in Scala is an expression, and has a return type

```scala
// Int
1 * 7

// Boolean
true == !false

// String
"Hello World"

// If statements have a return type
if ("February".contains("r")) 
  "it's going to be hot" 
else 
  "it's going to be cold"

```

---

### Values 

Values (things which do not change) can be declared with `val`

```scala
val pi:Double = 3.1415927
```

Type declarations come after the variable name. But Scala can also do "type inference", so we can often omit them

```scala
val pi = 3.1415927
```

---

### Variables

Variables (things which can be changed) can be declared with `var`

```scala
var radius:Int = 10
```

Again, we can omit the type annotation and the compiler will infer it

```scala
var radius = 10
```


---

### Functions

Functions can be declared with `def`

```scala
def circumference(r:Double):Double = 2 * pi * r
```

Notice that we say that the function *equals* its result

Again we can omit the type annotation on the function if we want

```scala
def circumference(r:Double) = 2 * pi * r
```

(Though I generally encourage explicitly declaring the return type of functions)

---

### Unit

What about functions that really don't have a result to return?

```scala
def sayHello(person:String):Unit = println("Hello " + person)
```

`Unit` is a type in Scala which is roughly equivalent to `void` in Java.

---

### Functions don't have to have arguments


```scala
def sayHello = println("Hello there!")
```

...but if a function has a side-effect, it's good practice to give it ()

```scala
def sayHello() = println("Hello there!")
```

---

### Named and default values

Functions can have default values, and arguments can be referred to by their names

```scala
def hello(name:String = "World") = println("hello " + name)

hello()

hello(name = "Class!")
```



---

### Loop with while

Scala has while loops. And like Java it is a "curly brace" language

```scala
var i = 0
while(i < 100) {
  if (i % 15 == 0) {
    println("fizzbuzz")
  } else if (i % 5 == 0) {
    println("buzz")
  } else if (i % 3 == 0) {
    println("fizz")
  } else println(i)
  i += 1
}
```

---

### Loop with do ... while

Scala has while loops. And like Java it is a "curly brace" language

```scala
var i = 0
do {
  if (i % 15 == 0) {
    println("fizzbuzz")
  } else if (i % 5 == 0) {
    println("buzz")
  } else if (i % 3 == 0) {
    println("fizz")
  } else println(i)
  i += 1
} while(i < 100) 
```

---

### Leading question -- any kind of loops I've missed?



---

### Arrays

Scala uses square brackets `[ ]` to parameterise types.

```scala
val names = new Array[String](3)
```

And round brackets `( )` to index into arrays

```scala
names(0) = "William"
```

Notice that although we declared `names` as a `val`, arrays are mutable. We can't point names to a different array, but we can change its contents.

---

### Syntactic sugar

Indexing into an array looks like it's a method call:

```scala
names(0)
```

And to the compiler, it *is* a method call, but on a special method called `apply`.

```scala
names.apply(0)
```

If you put the name of a variable, and then call it as if it were a function, Scala will try to call an `apply` method on it

---

### Update

Setting a value in an array is also syntactic sugar, for a call to `update`

```scala
names(0) = "Fred"
```

is equivalent to 

```scala
names.update(0, "Fred")
```

> when an assignment is made to a variable to which parentheses and one or more arguments have been applied, the compiler will transform that into an invocation of an update method that takes the arguments in parentheses as well as the object to the right of the equals sign -- Odersky & Spoon


---

### Any

Suppose we wanted to have an Array that could hold anything.

In Java, we have to have an `Array<Object>` and remember that primitive `int`s are being "autoboxed" to `Integers`

In Scala, there is a universal supertype: `Any`

```scala
val arrayOfAnything = new Array[Any](3)
arrayOfAnything(2) = 3
arrayOfAnything(2).toString
```

Behind the scenes it will still autobox (Scala is implemented on Java) but Scala does not have a distinction between `int` and `Integer`. It's just `Int`.


---

### Lists

Most functional programming languages have a `List` type -- a type for an *immutable* singly-linked list of data.

```scala
val names = List("Fiona", "Euan", "Rebecca")

// This won't work
names(1) = "Matthew"
```

---

### Lists

* An empty list comprises just the element `Nil`

* Other lists have a *head*, which is an element, and a *tail* which is a List

--

  * `Nil` - the empty list

--

  * `1 :: Nil` - 1 is the head, Nil is the tail

--

  * `2 :: 1 :: Nil` - 2 is the head, 1 :: Nil is the tail

---

### Appending Lists

```scala
val extendedNames = "Grace" :: names
```

Note that we haven't *changed* the list, we've created a *new* list that happens to have the old list as its *tail*

```scala
extendedNames.tail == names
```

---

### Empty lists

An empty list can be created in a few ways:

```scala
val emptyList:List[Int] = List()
val anotherEmptyList:List[Int] = Nil
val yetAnotherEmptyList:List[Int] = List.empty
```

But these all give us the same object (`Nil`).

---

### Seq

Where `List` is a particular struture, `Seq` is a *trait* that indicates a sequence of elements.

`List[Int[` is a `Seq[Int]`. But so is `Array[Int]`

--

If you just ask for a sequence of elements, the default version is immutable

```scala
val mySeq = Seq(1, 2, 3, 4) // will give an immutable Seq
```

but there are mutable subclasses of `Seq` as well.

---

### Set

`Set` meanwhile is more like a mathematical set -- unordered, and defined by which elements are in it (not how often they are repeated) 

```scala
val mySet = Set(1, 2, 3, 4)

mySet == Set(4, 4, 3, 3, 2, 2, 1, 1)
```

The default version is immutable. 

---

### Tuples

*Tuples* are pairs of objects. 

```scala
val tuple:(Int, String) = (1, "a")
```

That's a tuple of an Int and a String

We can have longer tuples, and we can miss out the type annotation

```scala 
val longerTuple = (1, "a", 2, "b", 3, "c")
```

---

### Syntactic sugar for tuples

Instead of `(1, 2)` we can also write `1 -> 2`

This helps make some code more legible. Instead of

```scala
Seq((1, 'a'), (2, 'b'), (3, 'c'))
```

we can write

```scala
Seq(1 -> 'a', 2 -> 'b', 3 -> 'c')
```

---

### Destructuring assignment

A fairly common pattern if you have a tuple is to extract it into variables

```scala
val myTuple = (1, "a")
val letter = myTuple._2 // can be inferred this is a String
```

You can also extract more than one variable at once

```scala
val (number, letter) = myTuple
```

---

### Destructuring assignment on arrays

It's not just tuples that can use destructuring assignment; lots of classes can.

For example, suppose I have an array passed to me from Java, and I know there's five of them at compile time.

```java
String[] names = new String { "Algernon", "Bertie", "Cecily", "Dahlia", "Earnest" }
```

```scala
val Array(a, b, c, d, e) = names
```

