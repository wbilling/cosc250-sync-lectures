class: center, middle

# Inheritance and Typeclasses

.byline[ *Will Billingsley* ]

---

## Inheritance -- what you've seen before

In Java, you're probably used to *nominal subtyping*.

We might declere

```java
public interface HasId { 
    String getId();
}
```

This declares a contract other classes can choose to fulfil.

---

## Nominal subtyping

In another JAR entirely, class `Foo` might declare it is a subtype of `HasId`
  
```java
public class Foo extends HasId {

    String id;
    
    @Override
    public String getId() { 
      return this.id;
    }  
}
```

A `Foo` is a `HasId`

---

## Nominal subtyping

But if class `Foo` doesn't declare it is a subtype of `HasId`, it is not a subtype of `HasId`, even if it contains the required method.

```java
public class Foo {

    String id;
    
    public String getId() { 
      return this.id;
    }  
}
```

A `Foo` is not a `HasId`

---

## Nominal subtypes in Scala

Scala can do this too

```scala
trait HasId {
  def id:String
}

case class Foo(id:String) extends HasId
```

--

But notice we were able to implement the *method* `id` using a *value* rather than needing a getter or setter.

In Scala, a value or a variable count as an accessor.

---

## Exploring how types vary

Let's suppose we have two kinds of User

```scala
class Post(id:String)

class User(val name:String) {
  def post(text:String):Post = ???
}

class Moderator(name:String) extends User(name) {
  def moderate() = ???
}
```

We're not going to define the methods because we're just exploring the type system -- whether things compile.

---

## Covariance

`Moderator` is a subtype of `User`

Is a `List[Moderator]` a `List[User]`?

--
```scala
var listUser:List[User] = List.empty
val listMod:List[Moderator] = List.empty

listMod.isInstanceOf[List[User]] // true

listUser = listMod // succeeds
```

--

`List` is covariant -- it is defined as `List[T+]`

If a `Moderator` is a `User`, a `List[Moderator]` is a `List[User]`. Anywhere you want a `List[User]`, you can use a `List[Moderator]`

---

## Invariance

`Moderator` is a subtype of `User`

Is a `Buffer[Moderator]` a `Buffer[User]`?

```scala
var bufUser:mutable.Buffer[User] = mutable.Buffer.empty
val bufMod:mutable.Buffer[Moderator] = mutable.Buffer.empty
```

--
```scala
bufMod.isInstanceOf[mutable.Buffer[User]] // true

bufUser = bufMod // Doesn't compile!
```

Clearly, `Buffer[T]` is not covariant, but why not?

And there's something else going on -- the runtime call to `isInstanceOf` succeeded, but the compile-time assignment failed.

---

## Why Buffer can't be covariant

`Buffer` is mutable. If a `Buffer[Moderator]` was a `Buffer[User]`, then we'd be able to do this:

```scala
val bufMod = mutable.Buffer.empty[Moderator]
val bufUser:mutable.Buffer[User] = bufMod

bufUser.append(new User("Not a moderator"))
// Oops, we just added a non-moderator to a buffer of moderators
```

--

or worse:

```scala
val bufAny:mutable.Buffer[Any] = bufMod
bufAny.append(7) // No, the number 7 is not a moderator...
```

--

If a container is mutable, generally you don't want it to be covariant.

---

## Type erasure

Ok, so why did the run-time check succeed, but the compile-time check fail?

```scala
bufMod.isInstanceOf[mutable.Buffer[User]] // true

bufUser = bufMod // Doesn't compile!
```

--

Java (and therefore Scala) uses *type erasure* for generics.

The compiler knows `Buffer[User]` is a Buffer of Users. But at runtime, it only knows it's a Buffer.

--

So, even this would return true at runtime:

```scala
bufMod.isInstanceOf[mutable.Buffer[String]]
```

---

## A consequence of type erasure

Though we can pattern match on classes, we can't pattern match on generics:

```scala
def patternMatchOnGenerics[T](list:List[T]):String = {
  list match {
    case integers:List[Int] => "ints"
    case _ => "not ints"
  }
}
```


--
Though that compiles, it doesn't work:

```scala
patternMatchOnGenerics(List("a", "b", "c"))
// ints
```


---

## Contravariance

`Moderator` is a subtype of `User`

Is a function that accepts `User`s a function that accepts `Moderator`s?

--

Verbally, that sounds like it should be yes. Translating it to code, should this compile?

```scala
def printName(u:User) = {
  println(u.name)
}

val myFunc: Moderator => Unit = printName
```

--
Yes! A function that accepts Users is a function that accepts Moderators!

---

## Contravariance

`Function` is *contravariant* in its arguments, and *covariant* in its result.

It's defined:

```scala
type Function[-A, +B] = A => B
```

--

For `Function[Foo, Bar]` to be a subtype of `Function[X, Y]`:

* The result `Bar` must be a `subtype` of `Y`. So code expecting a `Y` knows it can call methods defined on `Y`

* The argument `Foo` must be a `supertype` of `X`. So code expecting to be able to pass an `X` to the function can.

---

class: center, middle

## Two other kinds of subtype you might not have seen before

---

## Structural subtyping

We can do *structural subtyping* -- where we tell Scala we'd like to accept *any class* that has the method we're asking for, whether or not it declares it implements a trait.

--

```scala
object Demo {

  type HasId = { 
    def getId:String 
  }

  class Book(val title:String, val author:String) {
    def getId = ""
  }

  val a: HasId = new Book("a", "b")
}
```

---

## Structural subtyping

Structural subtyping isn't very common in Scala as it is quite inefficient.

---

## Typeclasses

Typeclasses are incredibly flexible and very efficient.

They are very common in Scala and Haskell.

But to explain them, I'm going to need to ask you a question...

---

## Typeclasses

Are these *Openable*?

* A can
* A beer bottle
* A wine bottle (with a cork)

---

## Typeclasses

Are these *Openable*?

* A can -- *only if I've got a can opener!*
* A beer bottle -- *only if I've got a bottle opener!*
* A wine bottle -- *only if I've got a corkscrew!*

---

## Typeclasses

Whether `A` is a subtype of `B` depends on whether there is another
item in scope. eg, the can opener.

In the case of programming languages, however, we consider this
object the *evidence* that `A` is a subtype of `B`

---

## Typeclasses

Let's declare a trait for the **evidence** that something is Openable

```scala
trait Openable[A] {
  def open(a:A): String
}
```

--

This is not yet openable:

```scala
class Wine(val name:String)
```

because I haven't declared my corkscrew

---

## Typeclasses

Let's create a function, needing something openable.

Note that we have a second argument list asking for an *implicit parameter*.

```scala
def pour[A](item: A)(implicit ev: Openable[A]) = {
  val contents = ev.open(item)
  println(contents)
  contents
}
```

--

Calling it on our Wine bottle fails -- we still don't have a corkscrew

```scala
val w = new Wine("Beaujolais") // we need a second parameter!
```

---

## Typeclasses

Let's provide the corkscrew. I've put it in the companion object to Wine, because that's one of the places the compiler will look for it.

```scala
public object Wine {
  implicit object Corkscrew extends Openable[Wine] {
    def open(w:Wine) = "Lovely " + w.name
  }
}
```

---

## Typeclasses

Now that we've declared our corkscrew, we can call our pour function, which was defined:

```scala
def pour[A](item: A)(implicit ev: Openable[A]) = {
  val contents = ev.open(item)
  println(contents)
  contents
}
```

--
Calling it with the corkscrew explicitly:

```scala
pour(new Wine("Beaujolais"))(Wine.Corkscrew)
```

--
or letting the compiler find the corkscrew implicitly

```scala
pour(new Wine("Beaujolais"))
```

---

## Syntactic sugar

This is common enough that there's another way of writing this

```scala
def pour[A](item: A)(implicit ev: Openable[A]) = {
  val contents = ev.open(item)
  println(contents)
  contents
}
```

Can be written:

--

```scala
def pour[A : Openable](item: A) = {
  val opener = implicitly[Openable[A]]
  val contents = opener.open(item)
  println(contents)
  contents
}
```
