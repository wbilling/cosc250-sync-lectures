class: center, middle

# Case Classes <br/> & Pattern Matching

.byline[ *Will Billingsley, CC-BY* ]

---

class: center, middle

## Let's start with a few more things about functions

---

## Partial application

You remember how `fold` has two argument lists? What if we just wanted to complete the first one...

```scala
val a = List(1, 2, 3, 4)
val f = a.fold(0)
```

--

Hmm, that doesn't compile, but if we give it the type annotation (tell it we really want a function), it does...

```scala
val a = List(1, 2, 3, 4)
val f:(((Int, Int) => Int) => Int) = a.fold(0)

f(_ + _) // res0: Int = 10
```

---

## Partial application

We can do this even within the same argument list

* Suppose we have

    ```scala
    // Euclid's algorithm
    @tailrec
    def gcd(a:Int, b:Int):Int = {
      if (b == 0) a else gcd(b, a % b)
    }
    ```

* We can ***partially apply*** this:

    ```scala
    val gcd21 = gcd(21, _ : Int)
    gcd21(14)    
    ```

---

## Currying

* Let's define `add` like this:
 
  ```scala
  def add(x:Int) = { y:Int => x + y }
  ```

  Now
  
  ```
  add(1)    // is a function
  add(1)(2) // is 3
  ```

* This is called Currying, after Haskell Curry...

--
  And in Haskell, all functions are curried!

  ```hs
  add     :: Integer -> Integer -> Integer
  add x y =  x + y
  ```

---

## What are you doing on...

Suppose we wanted a function that said what we were doing on a particular day

```scala
def whatAmIDoingOn(day:Day):String { 
  if (day == Monday || day == Wednesday) { 
    "Learning Scala!"
  }
}
```

That doesn't compile. Why not?

---

## Functions

What is *log(0)*?

Let's [Google it...](https://www.google.com.au?q=log+0)

--

### Partial and total functions

* A **total function** is defined over every element in its domain

* A **partial function** is only defined for part of its domain

--

Let's assume for now that we can only declare total functions. 

---

## The Java way (return null):

```java
public String whatAmIDoingOn(Day day) { 
  if (day == Monday || day == Wednesday) { 
    return "Learning Scala!";
  } else {
    return null;
  }
}
```
--

[The Billion Dollar Mistake](http://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare) &mdash; Tony Hoare

---

## Option

* The problem with null is ***any*** reference can be null. We're not communicating whether the value can be null or not.

* What if we made it explicit whether we'd always return a value, or only sometimes?

--

  > I hereby decree that I shall never return `null` <br/>
  > .right[ &mdash; *a Scala programmer's motto* ]

--

  Let's return an ***Option*** if it could be empty

---

## Option

* An `Option[T]` is either
    - `Some(item:T)`, or
    - `None`

--

```scala
def whatAmIDoingOn(Day day):Option[String] {
  if (day == Monday || day == Wednesday) {
    Some("Learning Scala")
  } else None
}
```

Now we have a total function &mdash; it's defined for every possibility.
  
The return type of *Option[String]* tells us there might or might not be a String in there.  

---

## Option

* What do *something* and *nothing* have in common (conceptually)?

--

* Conceptually, `Option[T]` is a ***disjoint union***  
--
  (but it's not actually implemented that way)

  Haskell's equivalent, `Maybe`, really is implemented as a disjoint union:
  
  ```hs
  data Maybe = Just a | Nothing
  ```

---

## Someone gave you an Option

* We could do this

  ```scala
  def subtractOne(opt:Option[Int]):Option[Int] = {
      if (opt.nonEmpty) {
        Some(opt.get - 1)
      } else None
  }
  ```

  but it's bad practice (never call `get` on an `Option`)
  
--

* really, we'd like to consider the ***case*** where it's something and the ***case*** where it's nothing...

---

## Pattern matching

* Let's consider the cases for that Option
    
    ```scala
    def subtractOne(opt:Option[Int]):Option[Int] = {
      opt match {
        case Some(i) => Some(i - 1)
        case None => None
      }
    }
    ```

--

* And often the compiler can tell us if we've considered all the cases!

---

## Pattern matching with Wildcards

* An underscore (`_`) on the left matches anything.
    
    ```scala
    def subtractOne(opt:Option[Int]):Option[Int] = {
      opt match {
        case Some(i) => Some(i - 1)
        case _ => _
      }
    }
    ```
  
  *"If it's some i, return some i minus one. For any other value, just return the value"*

---

## Pattern Matching Factorial

```scala
def factorial(i:Int):Int = i match {
  case x if x <= 1  => 1
  case _            => i * factorial(i - 1)
}
```

--
(but that's not tail recursive)


---

### How is Option implemented?

* `Option` is actually a `sealed abstract class`

  - ***sealed*** means its only subtypes are declared in the same file 

--
  - `Some[T]` is a ***case class*** that extends `Option[T]`

--

  - `None` is a ***case object*** that extends `Option[Nothing]`
--

* The reason it's different from Haskell is because Scala is object-oriented. It's *idiomatic* in OO languages to define methods on the parent class.

* `None` and `Some[T]` have methods in common...

---

### Case classes

```scala
case class Card(value:Int, suit:Char)
```

--

* Can be instantiated without saying `new`

    ```scala
    val aceOfSpades = Card(1, 'S')
    ```
--

  Scala automatically declares a method called *apply* on the companion object
  
  ```scala
  val alsoAceOfSpades = Card.apply(1, 'S')
  ```

  (it's a bit of *syntactic sugar* that you don't have to write the word "apply")

---

### Case classes

```scala
case class Card(value:Int, suit:Char)
```

--

* Export their constructor parameters

  ```scala
  println(aceOfSpades.suit)
  ```

--

* Comparison is by value

    ```scala
    Card(1, 'S') == aceOfSpades
    ```
--
   
  Scala automatically defines *equals* and *hashCode*

---

### Case classes

```scala
case class Card(value:Int, suit:Char)
```

--

* Can be decomposed using extractors

    ```scala
    val Card(x, y) = aceOfSpades // x: Int = 1 y: Char = S
    ```

  To do this, Scala automatically defines a method called *unapply* 

--
  
* This allows pattern matching on their contents

    ```scala
    card match {
      case Card(_, 'S') => "It's a Spade!"
    }
    ```

---
    
### None and Nothing

* `None` is a ***case object*** that extends `Option[Nothing]`

--

* Because it's a ***case object***, there is only one `None`

  ```scala
  var a:Option[String] = None
  var b:Option[Int] = None 
  ```
  
  These both use the same `None` object, even though one variable is of type `Option[Int]` and the other is an `Option[String]`
    
---

## None and Nothing

* `None` is a ***case object*** that extends `Option[Nothing]`

--

* `Nothing` is what mathematicians would call a ***bottom*** type. *"Nothing is a subtype of everything..."*  
--

  There is no value of type *Nothing* &mdash; it's an *uninhabited* type.
  
--

* Option[T] is *covariant* in T. (It's defined as `Option[+T]`)

  Because `Nothing` is a subtype of everything (any `T`),  
  `Option[Nothing]` is a subtype of any `Option[T]`  
  So, `None` is a valid value for any `Option[T]`


---

## Take a breath...

Ok, that was a bit of a head-scratcher... some types and complex machinery but it lets us do things like:

```scala
case class Card(value:Int, suit:Char)

def describe(card:Card):String = card match {
  case Card(1, _)                   => "An ace!"
  case Card(x, _) if (x > 10)       => "A picture card!"
  case Card(x, y)                   => s"Just a boring old $x of $y"
}
```

--

or even

```scala
def describe(card:Option[Card]):String = card match {
  case Some(Card(1, _))            => "An ace!"
  case Some(Card(x, _)) if x > 10  => "A picture card!"
  case Some(Card(x, y))            => s"Just a boring old $x of $y"
  case _                           => "You didn't give me a card!"
}
```


---

## Let's make Option even shorter?

* This style of code *would* be really common in Scala

    ```scala
    def myFunc(opt:Option[Int]) = {
      opt match {
        case Some(i) => // do something with i
        case None => None
      }
    }
    ```

  except it's needlessly verbose

--

* What if we could *just* say what we want to do with i? An option is like a list of zero or one elements...

--

```scala
opt.map(doSomethingWithI)
```
  
---

## Partial functions

* Way back at the start, I said "let's *assume* we can only define total functions"

* Actually in Scala, we *can* also define partial functions

    ```scala
    val whichPrime:PartialFunction[Int, Int] = {
      case 2 => 1
      case 3 => 2
      case 5 => 3
      case 7 => 4
    }
    whichPrime(3)             // 2
    whichPrime.isDefinedAt(4) // false
    ```

---

### `collect` on `List` with `case`

```scala
val mixedList = List(1, 2, "Fred", "Bob", 3)
mixedList.map { case x:Int => x * 2 }

// fails with a Match error (not all of them matched a case)
```

but

```scala
val mixedList = List(1, 2, "Fred", "Bob", 3)
mixedList.collect { case x:Int => x * 2 }

// produces List(2, 4, 6)
```
