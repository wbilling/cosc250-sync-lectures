class: center, middle

# Laziness and Streams

(NB: We'll meet another kind of stream next week)

.byline[ *Will Billingsley, CC-BY* ]

---

## Lazy vs Strict

* Analogy: A boy and his homework...

--

Strict: the computation is evaluated once, as soon as it is declared

Lazy: the computation is evaluated once, *only when it is used*

---

## def

Let's consider the following declaration:

```scala
def myValue:Int = {
  println("I was called")
  3
}
```

How many times will it print out `I was called` in the following code:

```scala
println(myValue)
```

---

## def

Let's consider the following declaration:

```scala
def myValue:Int = {
  println("I was called")
  3
}
```

How many times will it print out `I was called` in the following code:

```scala
println(myValue)
println(myValue)
```

---

## def

Let's consider the following declaration:

```scala
def myValue:Int = {
  println("I was called")
  3
}
```

How many times will it print out `I was called` in the following code:

```scala
// empty
```

---

## val

Let's change the definition:

```scala
val myValue:Int = {
  println("I was called")
  3
}
```

How many times will it print out `I was called` in the following code:

```scala
println(myValue)
```

---

## val

Let's change the definition:

```scala
val myValue:Int = {
  println("I was called")
  3
}
```

How many times will it print out `I was called` in the following code:

```scala
println(myValue)
println(myValue)
```

---

## val

Let's change the definition:

```scala
val myValue:Int = {
  println("I was called")
  3
}
```

How many times will it print out `I was called`:

```scala
// empty
```

---

### Laziness

What if we want it to run the calculation *exactly once*, but *only if it's needed*?

--
```scala
lazy val myValue:Int = {
  println("I was called")
  3
}
```

---

### Laziness

* We can declare a value that might involve a long computation

* It can be evaluated *exactly once*, only when needed, and *never recalculated*

* This means we really want it to be referentially transparent -- at least in the aspects we care about!

---

### Side discussion -- logs

* Print or any logging operation is an effect

* But we tend to think about it as a side-effect. When we log something, it's because we want to be able to debug it, not because we want to change the fundamental functionality of the program

---

### From a StackOverflow Haskell question

> It looks like the parser you're using is from the HCodecs package, which seems to be relatively limited, doesn't allow IO, and isn't defined as a monad transformer.
> 
> Honestly my advice would be to consider using a different parsing library. Parsec tends to be kind of the default choice, and I think attoparsec is popular for specific purposes (which might include what you're doing)

http://stackoverflow.com/questions/6310961/how-do-i-do-logging-in-haskell

---

### Pragmatic purity

* Really, to be allowed to log the output for debugging, the programmer needs to change their parser library?

* Separation of concerns 

* Change size

---

### Infinite Lists

* Suppose I have an unknown mathematical function `f: Int => Boolean`

* I would like to find the second occurrance in the natural numbers where `f(x)` is true.

```scala
// ???
val naturals = (1 to infinity)
naturals.filter(f)(2)
```

---

### Infinite Lists

* We can't really instantiate a list from 1 to infinity

* We don't know how much of the list is "enough" to get our second value, to instantiate anything smaller than the whole list.

---

### Lazy lists -- Streams

* We need a list-like structure that'll *generate* the next value lazily as long as we keep asking for it

* That sounds a lot like `lazy val` but for more than one value

--

* I wonder if there's an equivalent for `def` for more than one value?

---

### By-value and By-name 

* Earlier, we defined 
   * `def myValue` 
   * `val myValue`
   *  `lazy val myValue`

* What if we could do that for *function arguments*?

---

### By value function arguments

```scala
def myValue = {
  println("myValue was calculated")
  3
}
```

How many times is `myValue was calculated` printed in this:

```
def myFunction(i:Int) = {
  println(s"$i + 1 == ${i + 1}")
  println(s"$i + 2 == ${i + 2}")
}

myFunction(myValue)
```

---

### By name function arguments

```scala
def myValue = {
  println("myValue was calculated")
  3
}
```

How many times is `myValue was calculated` printed in this:

```
def myFunction(i: => Int) = {
  println(s"$i + 1 == ${i + 1}")
  println(s"$i + 2 == ${i + 2}")
}

myFunction(myValue)
```

---

### By-name, with a lazy val assignment

```scala
def myValue = {
  println("myValue was calculated")
  3
}
```

How many times is `myValue was calculated` printed in this:

```
def myFunction(_i: => Int) = {
  lazy val i = _i

  println(s"$i + 1 == ${i + 1}")
  println(s"$i + 2 == ${i + 2}")
}

myFunction(myValue)
```

--

### By-name, with a lazy val assignment

```scala
def myValue = {
  println("myValue was calculated")
  3
}
```

How many times is `myValue was calculated` printed in this:

```
def myFunction(_i: => Int) = {
  lazy val i = _i

  // nothing else
}

myFunction(myValue)
```

---

### Stream

* Using this machinery, we can build up a kind of *lazy list* called a Stream

* Let's start with a *strict* version, and see how we can change it

---

### Lst -- let's define a simplified List trait

```scala
  trait Lst[+T] {
    def tail:Lst[T]
    def head:T

    def apply(n:Int):T = {
      if (n < 0 || this == Nl) {
        throw new IndexOutOfBoundsException
      } else if (n == 0) {
        head
      } else {
        tail.apply(n -1)
      }
    }
  }
```

---

### We need a `Nil`

If we recall, `Nothing` is a bottom type, and we can just have a single object represent all empty `Lst`s.

```scala
  object Nl extends Lst[Nothing] {
    def tail = throw new NoSuchElementException("empty list has no tail")
    def head = throw new NoSuchElementException("empty list has no head")

    override def toString = "Nl"
  }
```


---

### A by-value (strict) implementation of `Lst`

In this implementation, everything is *by-value* (and strict)

```scala
  class Cons[T](val head:T, val tail: Lst[T]) extends Lst[T] {
    override def toString = s"($head, $tail)"
  }
```

---

### A range function to test it with

Let's define a *range* function that will generate the natural numbers between two values *and print them out when they are calculated*

```scala
def range(from:Int, to:Int):Lst[Int] = {
  if (to < from) {
    Nl
  } else {
    println(s"Created element for $from")
    new Cons(from, range(from + 1, to))
  }
}
```

---

### Let's test the by-value version

```scala
  val bnRange = Lst.range(1, 6)
  println(bnRange.apply(1))
```

```
Created element for 1
Created element for 2
Created element for 3
Created element for 4
Created element for 5
Created element for 6
2
```

---

### Let's change Cons to be by-name

```scala
  class Cons[T](val head:T, t: => Lst[T]) extends Lst[T] {
    def tail = t
    override def toString = s"($head, $tail)"
  }
```

The constructor parameter *t* is by-name, and the trait function *tail* is defined as returning it.

---

### Let's test the by-name version

```scala
  val bnRange = Lst.range(1, 6)
  println(bnRange.apply(1))
```

```
Created element for 1
Created element for 2
2
```

What's happening? Let's look at range again:

```scala
new Cons(from, range(from + 1, to))
```

The second argument is by-name, and bnRange.tail.tail was never asked for. So, `range(3, ...)` was never called.

---

### But there's a problem

```scala
  val bnRange = Lst.range(1, 6)
  println(bnRange.apply(1))
  println(bnRange.apply(1))
```

```
Created element for 1
Created element for 2
2
Created element for 2
2
```

We're *re-calculating* the tail every time we ask for it

---

### By-name with a lazy val

This fixes our problem:

```scala
  class Cons[T](val head:T, t: => Lst[T]) extends Lst[T] {
    lazy val tail = t
    override def toString = s"($head, $tail)"
  }
```

By making the tail a *lazy val*, it is calculated exactly once and only if asked for

---

### The lazy val works

```scala
  val bnRange = Lst.range(1, 6000000)
  println(bnRange.apply(1))
  println(bnRange.apply(1))
```

```
Created element for 1
Created element for 2
2
2
```

---

### But here's a different problem

```scala
  val bnRange = Lst.range(1, 6)
  println(bnRange)
```

```
(1, (2, (3, (4, (5, (6, Nl))))))
```

Println causes everything to be generated. Just as well I didn't put in six million.

---

### Stream in Scala

* Stream in Scala is like a lazy list, but with many of these issues resolved

* It is made up of

  * `Stream.empty[T]` (like our `Nl`)
  * `Stream.cons[T](head:T, tail: => T)`

* It's lazy (caches results), but println does not force calculation

```scala
println(Stream.range(1, 6000000))
```
```
Stream(1, ?)
```

---

### Infinite *Streams*

* Suppose I have an unknown mathematical function `f: Int => Boolean`

* I would like to find the second occurrance in the natural numbers where `f(x)` is true.

```scala
// Yes, this works
val naturals = Stream.from(1)
naturals.filter(f)(1)
```

---

### Infinite Streams

```scala
  def f(i:Int) = i % 60 == 0

  val naturals = Stream.from(1)

  println(naturals.filter(f)(1))
  println(naturals)
```

```
120
Stream(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, ?)
```

---

class: center, middle

### Ok, but that's all *synchronous*. What if our stream is Twitter messages, coming over the network, all arriving asynchronously?

Coming next week: Reactive Streams (Akka Streams) 