
class: center, middle

# Past exam questions

---

## 2016 Exam
(just the programming ones)

---

## Question 4 [10 marks]

a) What does it mean to say that a function is *pure*, and what is meant by *referential transparency*? Give an example in your answer. [2 marks]

b) Explain, with an example, what *tail recursion* is and when it can be achieved? [2 marks]

c) What does it mean for a calculation to be *lazy* or *strict*? [2 marks]

d) What is a *monad*? Include the monad laws in your answer, and *give* two examples of monads from the Scala libraries. [4 marks] 

---

## Question 5 [8 marks]

Pascal's triangle begins as follows:

```
        1
      1   1
    1   2   1
  1   2   2   1
1   3   4   3   1
```

Each number is the sum of the two numbers above it in the immediately preceeding row.

Write a recursive function *fib(n)* that will calculate the *n*th row of Pascal's triangle.

```scala
def fib(n:Int):Seq[Int] = ???
```

---

## Question 6 [6 marks]

a) What is a `Future` in Scala? Explain, with an example, what is it useful for, and how it relates to `Promise`. [2 marks]

b) What is an *Actor*? Give an example of how interaction between Actors might use Futures. [4 marks]

c) What is *backpressure* in a reactive stream, and why is it needed? [2 marks]

---

## Question 7 [6 marks]

Determine the result and the return type of the following code. `Source` and `Sink` are imported from Akka Streams, but `Stream` is from the Scala standard library

a) `List(1, 2, 3).zipWithIndex.map({ case (x, y) => x * y})` [1 mark]

b) `List(1, 2, 3).flatMap({ x => List(x, x)})` [1 mark]

b) `Stream.from(1)` [1 mark]

c) `Stream.from(1).map(_ * 2)` [1 mark]

d) `Source(1 to 10).to(Sink.fold[Int, Int](0)(_ + _))` [2 marks]

---

class: center, middle

## 2016 Special Exam

---

## Question 1 [10 marks]a)	What does it mean to say that a function is pure, and what is referential transparency? Give an example in your answer. [2 marks]b)	Explain, with an example, what tail recursion is, and when it can be achieved. [2 marks]c)	What does it mean for a calculation to be lazy or strict? [2 marks]d)	What is a monad? Include the monad laws in your answer and give two examples of monads from the Scala libraries. [4 marks]

---

## Question 2 [12 marks]
There is a puzzle sequence that goes```1           // 11 1         // the previous line had one ‘1’2 1         // the previous line had two ‘1’s1 2 1 1     // the previous line had one ‘2’ followed by one ‘1’1 1 1 2 2 1 // the previous line had one ‘1’, followed by one ‘2’,             // followed by two ‘1’s ```
Write a recursive function to produce this sequence. It need not be tail recursive.```def puzzleSeq(n:Int):Seq[Int] = ???```
---

## Question 5 [20 marks]
You have been asked to simulate the card game Spit in Scala. This will be a game played by your code, not a human player.Spit has two players, but it is a game based on speed (it is not turn based). Each player has five stacks of three cards placed face up in front of them (called the “layout” piles), and an additional eleven cards in a face-down stack (the players’ “decks”). When both players are ready, they each turn over the top card of their eleven-card deck. This creates two discard piles. The aim of the game is to discard all the cards from your layout piles as quickly as possible. At any time, if a player has a card at the top of one of their layout piles that is one higher or one lower than the card at the top of either discard pile, they may discard the card onto that discard pile. Many times, both players can discard a card onto the pile – the one to get there first gets the play and the other must take back their card.If neither player can make a play, the game is “stuck”, and the two players simultaneously turn over another card from their deck until a player can play.

---

## Question 5, continued [20 marks]a)	Describe how you would design an implementation of this game – you need not include detailed code at this point, but should describe your design in sufficient detail that the examiner could implement your program design without needing to make any significant structural decisions. For example, you should describe what messages and what Actors your program would include. [10 marks]b)	Give sample code showing how the discard piles would be implemented, and include the code for a player discarding a card onto it. Explain how this code prevents two players from discarding an 8 onto a 7 at the same time.

---

## Question 3 [12 marks]
a)	What is a Future in Scala? Explain, with an example, what they are useful for, and how they relate to Promise. [3 marks]b)	What is an Actor? Give a brief example of how an Actor may be implemented in your answer. [3 marks]c)	In the Akka framework, a subclass of Actor cannot be directly instantiated by your code. How do you create Actors using Akka? [3 marks]d)	What is backpressure in a reactive stream, and why is it needed? [3 marks]