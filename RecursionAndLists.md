class: center, middle

# Patterns, Recursion and Lists

---

## Pattern matching

Pattern matching is very common in functional programming. It lets us test whether variables match a certain pattern.

Let's first show it just with an `Int`

--

```scala
val x = 9
val s = x match {
    case 0 => "zero"
    case 1 => "one"
    case x if x > 0 && x % 2 == 0 => "something positive and even"
    case x if x < 0 && x % 2 == 1 => "something negative and odd"
    case _ => "something else"
}
println(s)
```

---

## Pattern matching and destructuring

However, we can also pattern match inside structures

```scala
val pos = (3, 5)
val s = pos match {
    case (x, y) if x > 0 && y > 0 => "top right quadrant"
    case (x, y) if x < 0 && y > 0 => "top left quadrant"
    case (x, y) if x > 0 && y < 0 => "bottom right quadrant"
    case (x, y) if x < 0 && y < 0 => "bottom left quadrant"
    case _ => "You're on an axis"
}
println(s)
```

---

### Pattern matching on Lists

That includes pattern matching on a List

```scala
val list = 1 :: 2 :: 3 :: 4 :: Nil
list match {
    case Nil => "The list was empty"
    case a :: Nil => "It only had one element"
    case a :: b :: tail if b % 2 == 0 => "The second element was even"
    case _ => "The second element was odd"
}
```

---

### Let's define our own List type, ListInt

```scala
sealed trait ListInt
case class ConsInt(head:Int, tail:ListInt) extends ListInt
case object EmptyListInt extends ListInt
```

We'll see more of `case` later, but for the moment, it means we can use it in `match`/`case` expressions.

--

We can now define `ListInt` values, eg:

```scala
val myList = ConsInt(1, ConsInt(2, ConsInt(3, EmptyListInt)))
```

--

* What is `myList.head`?

--

* What is `myList.tail`?

--

* What is `myList.tail.head`?

---

### Let's define a `length` function 

```scala
def length(l:ListInt):Int = {
    ???
}
```

---

### Let's define a `length` function 

```scala
def length(l:ListInt):Int = l match {
    case EmptyListInt => 0
    case ConsInt(head, tail) => ???
}
```

--

However many there are in tail, plus this one

---

### Let's define a `length` function 

```scala
def length(l:ListInt):Int = l match {
    case EmptyListInt => 0
    case ConsInt(head, tail) => length(tail) + 1
}
```

--

Is `length` tail recursive?

--

No. Once the recursive call is made, the function still has to add one to the result.
It's not "in tail position"

---

### Tail recursion?

Is `length` tail recursive now?

```scala
def length(l:ListInt):Int = l match {
    case EmptyListInt => 0
    case ConsInt(head, tail) => 1 + length(tail)
}
```

--

Still no. It's not about being *lexically* at the end of the function. Once the recursive call is made, the function still has to add one to the result. It's still not "in tail position"

---

### Apply

Let's consider something else. At the moment, we've got no way of indexing into a `ListInt`.
eg, we can't say `myList(3)`

--

```scala
trait ListInt {
  def apply(n:Int):Int = ???
}
```

---

### Apply

Let's consider something else. At the moment, we've got no way of indexing into a `ListInt`.
eg, we can't say `myList(3)`

```scala
trait ListInt {
  def apply(n:Int):Int = this match {
      case EmptyListInt => throw new IndexOutOfRangeException()
      case ConsInt(h, tail) => ???
  }
}
```

---

### Apply

Let's consider something else. At the moment, we've got no way of indexing into a `ListInt`.
eg, we can't say `myList(3)`

```scala
trait ListInt {
  def apply(n:Int):Int = this match {
      case EmptyListInt => throw new IndexOutOfRangeException()
      case ConsInt(h, tail) => if (n == 0) h else tail.apply(n - 1)
  }
}
```

--

Is this tail recursive?

--

Yes. If `tail.apply(n-1)` is called, its result can just be returned.
The recursive call is in tail position.

---

### Tail recursive?

We can ask the compiler to verify with an annotation

```scala
import scala.annotation.tailrec

trait ListInt {

  @tailrec
  def apply(n:Int):Int = this match {
      case EmptyListInt => throw new IndexOutOfRangeException()
      case ConsInt(h, tail) => if (n == 0) h else tail.apply(n - 1)
  }
}
```

---

class: center, middle

### Examples of recursion

---

### Repeat the elements in a list

--

```scala
def repeat[T](l:List[T]):List[T] = l match {
    case h :: t => h :: h :: t
    case _ => _
}
```

---

### Select only every second element in a list

--

```scala
def alternate[T](l:List[T]):List[T] = l match {
    case a :: b :: tail => b :: alternate(tail)
    case _ => Nil
}
```