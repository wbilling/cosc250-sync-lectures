
class: center, middle

# Scala.js

---

## Scala.js

* Scala normally compiles to bytecode for the JVM

* Instead, compile to JS code -- that can be run on Node or in the browser

* JS is untyped, but supports functions-as-values. It works well as a compile target.

---

## Scala.js

Things we'll use:

* A plugin for SBT to compile our code

* An optimiser to optimise our code.

--

But wait! That optimiser is optimising *JS*. Let's use a JS optimiser! (the Closure Compiler)

---

## Scala.js

Ok, if we're integrating with the JS ecosystem...

* We probably don't want the JVM to have to execute JS in our build system. Old versions of Java are *really slow* at running JS.

* So Scala.js can use Node to do JS work, such as running the Closure Compiler.

---

## Integrating our code with JavaScript

We'll probably want to be able to make use of all the JS that's out there...

* Interact with the HTML DOM

* JQuery, etc.

So, there are libraries for Scala.js that provide "facades" to these -- they introduce the type signatures that JS doesn't have.

---

## Using the DOM from Scala.js

From the Scala.js tutorial:

```scala
import org.scalajs.dom
import dom.document

def appendPar(targetNode: dom.Node, text: String): Unit = {
  val parNode = document.createElement("p")
  val textNode = document.createTextNode(text)
  parNode.appendChild(textNode)
  targetNode.appendChild(parNode)
}
```

---

## Using JQuery from Scala.js

From the Scala.js tutorial:

```scala
import org.querki.jquery._

$("body").append("<p>[message]</p>")
```

---

## Sharing code

* Scala is quite nice for asynchronous work, eg `Future[T]`

* Browser to server is asynchronous...

* Write code, and have it cross-compile to JVM and JS, so you can use the same types on the client and server.


