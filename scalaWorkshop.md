class: center, middle

# Scala workshop

28 Sep 2017

---

class: center, middle

# Warning! Live and unplanned

---

## Warning: Developers are opinionated

--

<blockquote class="twitter-tweet" data-conversation="none" data-cards="hidden" data-partner="tweetdeck"><p lang="en" dir="ltr">You are a better human than to be using Scala.</p>&mdash; Tony Morris (@dibblego) <a href="https://twitter.com/dibblego/status/1045295208655536131?ref_src=twsrc%5Etfw">September 27, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

--

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="en" dir="ltr">If you used Scala today, consider the better human you could be tomorrow.</p>&mdash; Tony Morris (@dibblego) <a href="https://twitter.com/dibblego/status/1040526296609054720?ref_src=twsrc%5Etfw">September 14, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

--

<blockquote class="twitter-tweet" data-conversation="none" data-cards="hidden" data-partner="tweetdeck"><p lang="en" dir="ltr">node.js is rubbish, which is different to being both rubbish and unethical.<br><br>Talking about rubbish software gets boring, given the quantity.</p>&mdash; Tony Morris (@dibblego) <a href="https://twitter.com/dibblego/status/1040582265632243714?ref_src=twsrc%5Etfw">September 14, 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

---

## But that's ok...

> “PHP is a minor evil perpetrated and created by incompetent amateurs, whereas Perl is a great and insidious evil, perpetrated by skilled but perverted professionals.”
 
Jon Ribbens

--

> “There are only two things wrong with C++. The initial concept and the implementation”

Bertrand Meyer

--

> “There are only two kinds of languages: the ones people complain about and the ones nobody uses.”

Bjarne Soustroup

---

## Languages and types

<table>
  <thead><tr><th></th><th>Imperative language</th><th>Functional language</th></tr></thead>
  <tbody>
    <tr>
      <th>Dynamically typed</th>
      <td>
        JavaScript <br />
        Python
      </td>
      <td>
        Lisp <br />
        Scheme <br />
        Clojure <br />
      </td>
    </tr>  
    <tr>
      <th>Statically typed</th>
      <td>
        Java <br />
        C<br/> C++<br/> C#
      </td>
      <td>
        Haskell <br />
        OCaml <br />
        Scala <br />
      </td>
    </tr>
  </tbody>
</table>

but ...

---

class: center, middle

## 1: The Basics

(20 mins)

---

## Summary

--

* Compiles to JVM or JS

--

* *Impure* functional programming

--

* Object oriented

--

* Simple, composable async

--

* Some similarities with TypeScript, but more powerful

---

## Expression-oriented

* Expressions, type inference

* `if` has a type

* val, var, optional type annotation

---

## Functions

* def, parameter lists

* `Unit`

* functions as values

* partial application

* compose

---

## Immutable data structures

* tuple

* destructuring assignment

* List

* Seq

* Map

* case class

---

## Simple functional programming

* pattern matching

* Option

---

## Higher order functions

* map

* flatMap

* implicit (sum)

---

## Let's extract a (trivial) csv

* Shell execution, `sys.process._`

---

# Laziness

(10 min)

* lazy val

* Stream

* fibonacci

---

# Futures

(10 min)

* Similar to ES6 Promise

* Parse github API

* Handy

---

## Concurrency oriented programming

(15 min)

* Akka - Erlang in Scala

* Tend not to need it directly (low-level)

---

# Reactive Streams

(10 min)

* asynchronous streams - source, sink, flow

* a few annoying concepts, but concise when you know them

---

# Play

(20 min)

* Web is a concurrent environment

* We like to stream things

* Let's do some websockets with Scala!

---

# Scala.js

(20 min)

* Scala also compiles to JavaScript

* Futures work the same way as ES6 promises

* Let's hit the web!

---

# Big Data

* It doesn't fit on one machine

--

# Fast Data

* It's streaming in

--

In both cases, you can't really do imperative programming

---

## Spark &mdash; Big data for Scala

```scala
val sparkConf = new SparkConf().setAppName("Log Query")
val sc = new SparkContext(sparkConf)

val dataSet = sc.parallelize(exampleApacheLogs)

dataSet.map(line => (extractKey(line), extractStats(line)))
      .reduceByKey((a, b) => a.merge(b))
      .collect()
      .foreach{
        case (user, query) => println("%s\t%s".format(user, query))
      }

sc.stop()
```

---

## Kafka &mdash; Fast data for Scala

```scala
val builder: StreamsBuilder = new StreamsBuilder
val textLines: KStream[String, String] = 
  builder.stream[String, String]("TextLinesTopic")

val wordCounts: KTable[String, Long] = textLines
  .flatMapValues(textLine => textLine.toLowerCase.split("\\W+"))
  .groupBy((_, word) => word)
  .count(Materialized.as("counts-store"))
  
wordCounts.toStream.to("WordsWithCountsTopic")
```

